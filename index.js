const getCube = function(number){
    let cubeAnswer = number ** 3
    return(`The Cube of ${number} is ${cubeAnswer}`)
}

console.log(getCube(2))

const address = [258, 90011]
const [addressNumber,zipCode] = address

console.log(`I live at  ${addressNumber} Washington Ave NW, California ${zipCode}`)

const animal = {
    name: "Lolong",
    specie: "saltwater crocodile",
    weight: 1075,
    measurement: "20 ft 3 in",
}
const {name, specie, weight, measurement} = animal

console.log(`${name} was a ${specie}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)

const array = [1, 2, 3, 4, 5];

array.forEach((number) => console.log(number))

const reduceNumber = array.reduce((acc, num) => {
    return acc + num;
});

console.log(reduceNumber);

class Dog {
    constructor(name,age,breed) {
        this.name = name
        this.age = age
        this.breed = breed
    }
}

const buddy = new Dog("Frankie", 5, "Miniature Dachshund")

console.log(buddy)

